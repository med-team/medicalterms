# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

## [20230905]
### Changed
* Move project from github.com to codeberg.org
* Use UTF-8 for hunspell dictionary
* Update copyright years


## [20220425]
### Changed
* Use HTTPS URLs where available
* Convert Changelog to markdown and use format suggested by Keep a Changelog.
* Update autotools files
* Change maintainer's email address
* Update copyright years


## [20160103]
### Changed
* Update copyright years

### Fixed
* Do not depend on LC_ALL of build environment.
  Closes [Debian BTS #793813](https://bugs.debian.org/793813).


## [20110608]
### Added
* Add new words

### Changed
* Update copyright years

### Fixed
* Fix error in perl expression which resulted in an FTBFS.
  Closes [Debian BTS #628251](https://bugs.debian.org/628251).


## [20100204]
### Added
* Add new words

### Changed
* Update copyright years

### Fixed
* Install hunspell dictionaries in correct path. Thanks to Rene Engelhard
  for reporting this bug. Closes [Debian BTS #568329](https://bugs.debian.org/568329).


## [20091008]
### Added
* Add new words


## [20090825]
### Changed
* Install hunspell dictionaries in /usr/share/hunspell instead of
  /usr/share/myspell


## [20090216]
### Added
* Add new words
* New target "sort-all" for sorting all dictionaries

### Changed
* Update copyright years


## [20080712]
### Added
* Add new words

### Changed
* Move "Dermographie" to old spelling and add "Dermografie"
* Remove left over old spelling from adjektive-\*.txt
* Change maintainer name


## [20080620]
### Added
* Add new words
* Add compounding rules from igerman98
* Add script to convert wordlists to igerman98 format
* Add new Makefile target to create a contrib tarball for
  igerman98. Please note that the support of igerman98 is
  currently considered to be in alpha status.

### Changed
* Update README to mention hunspell instead of aspell
* Rename wordlist files to match the naming scheme of igerman98


## [20080515]
### Added
* Add new words

### Changed
* Regenerate auto* files with current toolchain
* Support a supplemental hunspell dictionary which needs a "base"
  dictionary

### Removed
* Remove support for aspell because that program does not support
  supplemental dictionaries like hunspell


## [20080327]
### Added
* Add new words for ophthalmological topics

### Changed
* Move project from gna.org to launchpad.net

### Fixed
* Fix some spelling errors
* Correct installation path for aspell dictionary


## [20080229]
### Added
* Some additions to the word lists

### Changed
* The aspell related targets in the Makefile have been renamed to
  include the string "-de-medical", to better reflect the medical
  domain of the generated files.
* The package is now licensed under GPL version 3 or later
* As the revision control system has been switched from Arch to
  Bazaar, some information has been updated in README. Moreover,
  the automatically generated ChangeLog is no longer available.
  Therefore, the package uses the "foreign" ruleset instead of
  "gnu" for configuring.
* Rename this file from NEWS to ChangeLog

### Fixed
* Do not install data with user and group set to root, but leave
  as the uid and gid which is executing the command


## [20051025]
### Added
* Some additions to the word lists; one incorrect entry has
  been removed

### Changed
* The macro AM_MAINTAINER_MODE is now enabled in configure.ac
* Changed the format of the NEWS file, so that the main changes
  between releases can be found at once


## [20050703]
### Added
* New category of words: generic names for most usual drugs
  are now included
* Created this NEWS file which makes the package suitable for
  "gnu" rules in the configure.ac file instead of "foreign".
  In addition, the NEWS file is checked for the current release
  number before actually shipping the tarball.

### Changed
* In the already distributed word lists were some typo fixes
  applied and quite a few more words added

### Fixed
* Fixed two errors in the Makefile, which made the creation of
  distribution packages really annoying


## [20050510]
* Initial release of the package


[20230905]: https://codeberg.org/toddy/medicalterms/compare/v20220425...v20230905
[20220425]: https://codeberg.org/toddy/medicalterms/compare/v20160103...v20220425
[20160103]: https://codeberg.org/toddy/medicalterms/compare/v20110608...v20160103
[20110608]: https://codeberg.org/toddy/medicalterms/compare/v20100204...v20110608
[20100204]: https://codeberg.org/toddy/medicalterms/compare/v20091008...v20100204
[20091008]: https://codeberg.org/toddy/medicalterms/compare/v20090825...v20091008
[20090825]: https://codeberg.org/toddy/medicalterms/compare/v20090216...v20090825
[20090216]: https://codeberg.org/toddy/medicalterms/compare/v20080712...v20090216
[20080712]: https://codeberg.org/toddy/medicalterms/compare/v20080620...v20080712
[20080620]: https://codeberg.org/toddy/medicalterms/compare/v20080515...v20080620
[20080515]: https://codeberg.org/toddy/medicalterms/compare/v20080327...v20080515
[20080327]: https://codeberg.org/toddy/medicalterms/compare/v20080229...v20080327
[20080229]: https://codeberg.org/toddy/medicalterms/compare/v20051025...v20080229
[20051025]: https://codeberg.org/toddy/medicalterms/compare/v20050703...v20051025
[20050703]: https://codeberg.org/toddy/medicalterms/compare/v20050510...v20050703
[20050510]: https://codeberg.org/toddy/medicalterms/releases/tag/v20050510
